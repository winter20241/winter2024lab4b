public class Cats{
	private int cutenessOfCat;
	private String textureOfCat;
	private String nameOfCat;
	
	public Cats(int cutenessOfCat, String textureOfCat, String nameOfCat){
		this.cutenessOfCat = cutenessOfCat;
		this.textureOfCat = textureOfCat;
		this.nameOfCat = nameOfCat;
	}
	
	public void setCutenessOfCat(int cutenessOfCat) {
		this.cutenessOfCat = cutenessOfCat;
	}
	public int getCutenessOfCat() {
		return this.cutenessOfCat;
	}
	
	public String getTextureOfCat() {
		return this.textureOfCat;
	}
	
	public String getNameOfCat() {
		return this.nameOfCat;
	}
	
}